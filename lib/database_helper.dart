import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'models/sound.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:io';
import 'dart:typed_data';
import 'dart:developer' as developer;

// singleton class to manage the database
class DatabaseHelper {
  // This is the actual database filename that is saved in the docs directory.
  static final _databaseName = "vocal.db";
  // Increment this version when you need to change the schema.
  //static final _databaseVersion = 2;

  // Make this a singleton class.
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  // Only allow a single open connection to the database.
  static Database? _database;
  Future<Database> get database async {
    await _copyDatabaseFromAssets();
    if (_database != null) {
      return _database!;
    }
    _database = await _initDatabase();
    return _database!;
  }

  _copyDatabaseFromAssets() async {
    var databasesPath = await getDatabasesPath();
    developer.log(databasesPath);
    String path = join(databasesPath, _databaseName);
    File dbFile = File(path);
    int dbSize = 0;
    // Check if the database exists
    var exists = await databaseExists(path);

    if (!exists) {
      // Make sure the parent directory exists
      try {
        await Directory(dirname(path)).create(recursive: true);
      } catch (_) {}
    }
    else
      dbSize = await dbFile.length();
    ByteData data =
    await rootBundle.load(join("assets", "database", _databaseName));
    // Copy from asset
    if (data.lengthInBytes > dbSize) {
      List<int> bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      // Write and flush the bytes written
      await dbFile.writeAsBytes(bytes, flush: true);
    }
  }

  // open the database
  _initDatabase() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, _databaseName);

    return await openDatabase(path);
  }

  // Database helper methods:
  Future<int> insert(Sound p) async {
    Database db = await database;
    int id = await db.insert('product', p.toMap());
    return id;
  }

  void showTables() async {
    Database db = await database;
    List<Map> list =
    await db.rawQuery('SELECT * FROM sqlite_master WHERE type="table";');
    print(list);
  }

  Future<List<Sound>> queryAll() async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM sound ORDER BY id ASC');
    List<Sound> pArr = [];

    for (var m in maps) {
      //print(m.toString());
      Sound p = Sound.fromMap(m);
      pArr.add(p);
    }
    return pArr;
  }

  Future<List<Sound>> filterByCategory(String category) async {
    Database db = await database;
    List<Map<String, dynamic>> maps = await db.rawQuery('SELECT * FROM sound WHERE primary_category = ? ORDER BY id ASC', [category]);
    List<Sound> pArr = [];

    for (var m in maps) {
      //print(m.toString());
      Sound p = Sound.fromMap(m);
      pArr.add(p);
    }
    return pArr;
  }
}
