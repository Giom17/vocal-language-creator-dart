import 'package:flutter/material.dart';
import 'package:language_creator/components/navbar.dart';
import 'package:language_creator/components/vocal_table.dart';

class Home extends StatelessWidget {

  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Navbar(),
      body: VocalTable(soundCategory: 'Vowels'),
    );
  }
}