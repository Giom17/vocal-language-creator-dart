import 'package:flutter/material.dart';
import 'package:just_audio/just_audio.dart';


class ButtonPlay extends StatefulWidget {

  const ButtonPlay({Key? key, required this.file}) : super(key: key);
  final String file;
  @override
  State<ButtonPlay> createState() => _ButtonPlayState();

}

class _ButtonPlayState extends State<ButtonPlay> {

  final AudioPlayer player = AudioPlayer();
  @override
  initState()  {
    super.initState();
    _initializePlayer();
  }
  Future<void> _initializePlayer() async {
    var setAsset = await player.setAsset('/assets/ogg/' + widget.file);
  }

  void playFile ()
  {
    player.play();
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.play_arrow_sharp),
      tooltip: 'Play',
      onPressed: playFile,
    );
  }
}