import 'package:flutter/material.dart';

class Navbar extends StatelessWidget implements PreferredSizeWidget {
  final double height = 50;

  const Navbar({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: const Text('Test'),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);

}
