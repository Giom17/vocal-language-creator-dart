import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:just_audio/just_audio.dart';
import 'package:language_creator/models/sound.dart';
import 'package:language_creator/components/button_play.dart';
import 'package:language_creator/database_helper.dart';



// https://phoible.org/inventories/view/162#tipa

class VocalTable extends StatefulWidget {

  const VocalTable({Key? key, required this.soundCategory}) : super(key: key);
  final String soundCategory;
  @override
  State<VocalTable> createState() => _VocalTableState();

}

class _VocalTableState extends State<VocalTable> {
  List<Sound> items = [];
  final DatabaseHelper _db = DatabaseHelper.instance;

  @override
  initState()  {
    super.initState();
    _initializeItems();
  }
  Future<void> _initializeItems() async {
    List<Sound> tmp = await _db.filterByCategory(widget.soundCategory);
    setState(() {
      items = tmp;
    });
  }

  List<TableRow> createRows()
  {
    List<TableRow> list = [];
    for (int i = 0; i < items.length; i++)
    {
      TableRow row = TableRow(
          children: [
            TableCell(
              child: Text(items[i].symbol)
            ),
            TableCell(
              child: Text(items[i].url)
            ),
            TableCell(
                child: ButtonPlay(file: items[i].audioFile)
            )
          ]
      );
      list.add(row);
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Table(
      border: TableBorder.all(),
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: createRows(),
    );
  }

}