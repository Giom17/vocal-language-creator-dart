class Sound {
  late int id;
  late String symbol;
  late String name;
  late String audioFile;
  late String primaryCategory;
  late String secondaryCategory;
  late String tertiaryCategory;
  late bool voiced;
  late bool rounded;
  late String url;

  // default constructor
  Sound(
      { required this.id,
        required this.symbol,
        required this.name,
        required this.audioFile,
        required this.primaryCategory,
        required this.secondaryCategory,
        required this.tertiaryCategory,
        required this.voiced,
        required this.rounded,
        required this.url});

  Map<String, dynamic> toMap() => {
    "id": id,
    "symbol": symbol,
    "name": name,
    "audioFile": audioFile,
    "primary_category": primaryCategory,
    "secondary_category": secondaryCategory,
    "tertiary_category": tertiaryCategory,
    "voiced": voiced,
    "rounded": rounded,
    "url": url,
  };

  // constructor
  Sound.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    symbol = map['symbol'];
    name = map['name'];
    audioFile = map['audio_file'];
    primaryCategory = map['primary_category'];
    secondaryCategory = map['secondary_category'];
    tertiaryCategory = map['tertiary_category'];
    voiced = (map['voiced'] == 0 ? false : true);
    rounded = (map['rounded'] == 0 ? false : true);
    url = map['url'];
  }

}
